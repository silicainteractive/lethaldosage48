﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LD48.Utils{
    public interface EnemyBehaviour {
        public void Behave();
    }
}
