﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SilicaLib.Core;

namespace LD48 {
    class PlayState : SilState{
        Sprites.Player player;

        public override void Create() {
            base.Create();
            player = new Sprites.Player();
            AddChild(player);
        }
    }
}
