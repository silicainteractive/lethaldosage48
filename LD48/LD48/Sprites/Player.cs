﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SilicaLib.Core;
using SilicaLib.Utils;
using Microsoft.Xna.Framework.Input;

namespace LD48.Sprites {
    public class Player : SilSprite {

        public Player()
            : base(new Vector2(50, 50)) { //HACK
            speed = 1;
            LoadGraphic("Grue", new Vector2(56, 71));
            maxVelocity = Vector2.One * (speed * 4);
            drag = Vector2.One * (speed * .2f);
            game.opaque.AddCommand("speed", "modify the speed of the player", null, delegate(Command.CommandArgs arguments) {
                if (arguments != null) {
                    string[] args = arguments.arguments.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (args.Length > 1) {
                        game.opaque.Log("Can only handle one argument.");
                        return;
                    }
                    if (!float.TryParse(args[0], out speed)) {
                        game.opaque.Log("please enter a numeric value.");
                        return;
                    }
                    game.opaque.Log("speed changed to {0}", speed);
                } else {
                    game.opaque.Log("what do you expect with no arguments?");
                }
            });
        }
        public override void Update(GameTime gameTime) {
            acceleration = Vector2.Zero;
            if (SilU.IsKeyDown(new Keys[] { Keys.W, Keys.Up })) {
                acceleration.Y = -speed;
            }
            if (SilU.IsKeyDown(new Keys[] { Keys.S, Keys.Down })) {
                acceleration.Y = speed;
            }
            if (SilU.IsKeyDown(new Keys[] { Keys.A, Keys.Left })) {
                acceleration.X = -speed;
            }
            if (SilU.IsKeyDown(new Keys[] { Keys.D, Keys.Right })) {
                acceleration.X = speed;
            }
            if (SilU.IsKeyDown(Keys.Space)) {
                Fire();
            }
            base.Update(gameTime);
        }
        public void Fire() {
            game.opaque.Log("pew pew pew!");
        }
        public override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch sb) {
            base.Draw(gameTime, sb);
        }
    }
}
